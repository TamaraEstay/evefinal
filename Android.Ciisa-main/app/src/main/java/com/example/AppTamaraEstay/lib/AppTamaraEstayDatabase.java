package com.example.AppTamaraEstay.lib;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.AppTamaraEstay.dao.BmiDao;
import com.example.AppTamaraEstay.dao.UserDao;
import com.example.AppTamaraEstay.models.BmiEntity;
import com.example.AppTamaraEstay.models.UserEntity;
import com.example.AppTamaraEstay.utils.Converters;

@Database(entities = {UserEntity.class, BmiEntity.class}, version = 2, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AppTamaraEstayDatabase extends RoomDatabase {
    private static final String DB_NAME = "AppTamaraEstay";
    private static AppTamaraEstayDatabase instance;

    public static synchronized AppTamaraEstayDatabase getInstance(Context ctx) {
        if (instance == null) {
            instance = Room.databaseBuilder(ctx.getApplicationContext(), AppTamaraEstayDatabase.class, DB_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract UserDao userDao();
    public abstract BmiDao bmiDao();
}
